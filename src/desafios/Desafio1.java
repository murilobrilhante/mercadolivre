package desafios;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.UUID;
import java.sql.Connection;  
import java.sql.PreparedStatement; 
import desafios.ConnectionBD;
import desafios.sendEmail;

public class Desafio1 {
	public static void main(String[] args) throws Exception  {
		   
		File arquivoCSV = new File("C:\\Users\\MuriloBrilhante\\Documents\\clientes.csv");
		
	        try{
	            Scanner reader = new Scanner(arquivoCSV);
	            
	            String linhasDoArquivo = new String();
	            
	            while(reader.hasNext()){
	                
	                linhasDoArquivo = reader.nextLine();
	                
	                /*CSV Infos*/
	                String[] dataFound = linhasDoArquivo.split(",");
	                String name = dataFound[0];
	                String lastname = dataFound[1];
	                String email = dataFound[2];
	                
	                /*Password*/
	                UUID uuid = UUID.randomUUID();
	                String myPassword = uuid.toString().substring(0,10);
	                
	                Connection conn = null;
	                PreparedStatement pstm = null;
	                
	                /*Conex�o*/
	                conn = ConnectionBD.createConnectionToMySQL();
	                
	                String sql = "insert into table_desafio1_cad" +
	                        " (name, last, email, password, acesso)" +
	                        " values (?,?,?,?,?)";
	                PreparedStatement stmt = conn.prepareStatement(sql);

	                /*Valores*/
	                stmt.setString(1, name);
	                stmt.setString(2, lastname);
	                stmt.setString(3, email);
	                stmt.setString(4, myPassword);
	                stmt.setString(1, "1");	  	   

	                /*Executa*/
	                stmt.execute();
	                stmt.close();
	                
	                sendEmail sm = new sendEmail();

	                sm.sendEmail("", email, "Confira sua senha", myPassword);

	                System.out.println("Gravado!");
	                
	            }
	        
	        }catch(FileNotFoundException e){
	        	
	        }
	            	
	}

}

