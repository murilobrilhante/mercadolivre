package desafios;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.util.Properties;
import desafios.ConnectionBD;

import javax.mail.Address;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.Session;
import javax.mail.Store;

public class Desafio2 {

   public static void check(String host, String storeType, String user,
      String password) 
   {
      try {

    	  Properties properties = new Properties();

      properties.put("mail.pop3.host", host);
      properties.put("mail.pop3.port", "995");
      properties.put("mail.pop3.starttls.enable", "true");
      Session emailSession = Session.getDefaultInstance(properties);
  
      Store store = emailSession.getStore("pop3s");

      store.connect(host, user, password);

      Folder emailFolder = store.getFolder("INBOX");
      emailFolder.open(Folder.READ_ONLY);

      Message[] messages = emailFolder.getMessages();

      for (int i = 0, n = messages.length; i < n; i++) {
    	  
         Message message = messages[i];
         String assuntoEmail = message.getSubject();
         String corpoEmail = message.getContent().toString();
         Address remetenteEmail = message.getFrom()[0];
         Date dataEmail = (Date) message.getReceivedDate();         
         
         if (assuntoEmail.contains("DevOps") || corpoEmail.contains("DevOps")){
        	 Connection conn = null;
             PreparedStatement pstm = null;
             
             /*Conex�o*/
             conn = ConnectionBD.createConnectionToMySQL();
             
             String sql = "insert into table_desafio2_cad" +
                     " (date, origin, subject)" +
                     " values (?,?,?)";
             PreparedStatement stmt = conn.prepareStatement(sql);

             /*Valores*/
             stmt.setString(1, dataEmail);
             stmt.setString(2, remetenteEmail);
             stmt.setString(3, assuntoEmail);  	   

             /*Executa*/
             stmt.execute();
             stmt.close();
			}
      }

      emailFolder.close(false);
      store.close();

      } catch (NoSuchProviderException e) {
         e.printStackTrace();
      } catch (MessagingException e) {
         e.printStackTrace();
      } catch (Exception e) {
         e.printStackTrace();
      }
   }

   public static void main(String[] args) {

      String host = "pop.gmail.com";
      String mailStoreType = "pop3";
      String username = "email@gmail.com";
      String password = "senha";

      check(host, mailStoreType, username, password);

   }

}